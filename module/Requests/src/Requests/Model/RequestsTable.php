<?php
namespace Requests\Model;

use Zend\Db\TableGateway\TableGateway;

class RequestsTable
{
    protected $tableGateway;
	protected $quotesTable;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getRequests($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    
    public function getUserOpenRequests($sm, $userId) {
        $adapter = $sm->get('Zend\Db\Adapter\Adapter');
		
	    $stmt = $adapter->createStatement("select *
											from requests r
											where r.userId = $userId");
		$results=$stmt->execute();
	
		$i = 0;
		$data = array();
		foreach($results as $result) {
			$r = new Requests();
			$r->exchangeArray($result);
			
			//get quotes example from another table
			//$r->quotes = $this->getQuotesTable($sm)->getQuotesForRequest($sm, $r->id);

			$data[$i] = $r;
			$i++;
		}
		
		return $data;
    }


    public function saveRequests(Requests $request)
    {
        $data = array(
            'name'  => $request->name,
        );

        $id = (int)$request->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getRequests($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function deleteRequests($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
}