<?php
namespace Requests\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class RequestsForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('requests');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'pickupFlightNo',
            'type' => 'Text',
            'options' => array(
                'label' => 'Flight No',
            ),
        ));
        $this->add(array(
            'name' => 'destFlightNo',
            'type' => 'Text',
            'options' => array(
                'label' => 'Flight No',
            ),
        ));
        $this->add(array(
            'name' => 'pickupAddress',
            'type' => 'Text',
            'options' => array(
                'label' => 'Address From',
            ),
        ));
        $this->add(array(
            'name' => 'destAddress',
            'type' => 'Text',
            'options' => array(
                'label' => 'Address Destination',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country',
            'options' => array(
                'label' => 'Country',
                'value_options' => array(
                    '1' => 'Malta'
                ),
            ),
            'attributes' => array(
                'value' => '1' //set selected to '1'
            )
        ));
		$this->add(array(
            'name' => 'dateForRequest',
            'type' => 'Date',
            'options' => array(
                'label' => 'Date',
            ),
            'attributes' => array(
                'id' => 'datepicker',
            ),
        ));$this->add(array(
            'name' => 'timeForRequest',
            'type' => 'DateTime',
            'options' => array(
                'label' => 'Time',
            ),
        ));$this->add(array(
            'name' => 'passengerNo',
            'type' => 'Text',
            'options' => array(
                'label' => 'Number of Passengers',
            ),
        ));$this->add(array(
            'name' => 'extraInfo',
            'type' => 'Text',
            'options' => array(
                'label' => 'Additional Information',
            ),
        ));
        $this->add(array(
            'name' => 'pickupAirport',
            'type' => 'Checkbox',
            'options' => array(
                'label' => 'Airport',
                'use_hidden_element' => false,
            ),
            'attributes' => array(
                'id' => 'airport',
            ),
        ));
        $this->add(array(
            'name' => 'destAirport',
            'type' => 'Checkbox',
            'options' => array(
                'label' => 'Airport',
                'use_hidden_element' => false,
            ),
            'attributes' => array(
                'id' => 'airportdest',
            ),
        ));
        $this->add(array(
            'name' => 'pickupSeaTerminal',
            'type' => 'Checkbox',
            'options' => array(
                'label' => 'Sea Terminal',
                'use_hidden_element' => false,
            ),
            'attributes' => array(
                'id' => 'pickupSeaTerminal',
            ),
        ));
        $this->add(array(
            'name' => 'destSeaTerminal',
            'type' => 'Checkbox',
            'options' => array(
                'label' => 'Sea Terminal',
                'use_hidden_element' => false,
            ),
            'attributes' => array(
                'id' => 'destSeaTerminal',
            ),
        ));
        $this->add(array(
            'name' => 'pickupTerminalNo',
            'type' => 'Text',
            'options' => array(
                'label' => 'Terminal Name / No',
            ),
        ));
        $this->add(array(
            'name' => 'destTerminalNo',
            'type' => 'Text',
            'options' => array(
                'label' => 'Terminal Name / No',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}