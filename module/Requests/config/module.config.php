<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Requests\Controller\Requests' => 'Requests\Controller\RequestsController',
            'Requests\Controller\RequestsService' => 'Requests\Controller\RequestsServiceController',
        ),
    ),
    
    'router' => array(
        'routes' => array(
            'requests' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/taximalta[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Requests\Controller\Requests',
                        'action'     => 'index',
                    ),
                ),
            ),
            'restful-requests-service' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/requestsservice',
            		'defaults' => array(
            			'controller' => 'Requests\Controller\RequestsService',	
            		),
            	),
            ),
        ),
    ),
    
    'view_manager' => array(
        'template_path_stack' => array(
            'requests' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);