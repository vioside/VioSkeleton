<?php
namespace Services;


use Services\Model\Call;
use Services\Model\CallTable;
use Services\Model\Rating;
use Services\Model\RatingTable;
use Services\Model\RoadClient;
use Services\Model\RoadClientTable;
use Services\Model\Driver;
use Services\Model\DriverTable;
use Services\Model\CallsDrivers;
use Services\Model\CallsDriversTable;
use Services\Model\Price;
use Services\Model\PriceTable;
use Services\Model\Token;
use Services\Model\TokenTable;
use Services\Model\Towns;
use Services\Model\TownsTable;
use Services\Model\ClientCoOrdinate;
use Services\Model\ClientCoOrdinateTable;
use Services\Model\DriverCoOrdinate;
use Services\Model\DriverCoOrdinateTable;


use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Services\Model\CallTable' =>  function($sm) {
                    $tableGateway = $sm->get('CallTableGateway');
                    $table = new CallTable($tableGateway);
                    return $table;
                },
                'CallTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Call());
                    return new TableGateway('calls', $dbAdapter, null, $resultSetPrototype);
                },
                'Services\Model\RatingTable' =>  function($sm) {
                    $tableGateway = $sm->get('RatingTableGateway');
                    $table = new RatingTable($tableGateway);
                    return $table;
                },
                'RatingTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Rating());
                    return new TableGateway('ratings', $dbAdapter, null, $resultSetPrototype);
                },
                'Services\Model\RoadClientTable' =>  function($sm) {
                    $tableGateway = $sm->get('RoadClientTableGateway');
                    $table = new RoadClientTable($tableGateway);
                    return $table;
                },
                'RoadClientTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new RoadClient());
                    return new TableGateway('roadclients', $dbAdapter, null, $resultSetPrototype);
                },
                'Services\Model\DriverTable' =>  function($sm) {
                    $tableGateway = $sm->get('DriverTableGateway');
                    $table = new DriverTable($tableGateway);
                    return $table;
                },
                'DriverTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Driver());
                    return new TableGateway('driver', $dbAdapter, null, $resultSetPrototype);
                },
                'Services\Model\CallsDriversTable' =>  function($sm) {
                    $tableGateway = $sm->get('CallsDriversTableGateway');
                    $table = new CallsDriversTable($tableGateway);
                    return $table;
                },
                'CallsDriversTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CallsDrivers());
                    return new TableGateway('callsDrivers', $dbAdapter, null, $resultSetPrototype);
                },
                
                'Services\Model\PriceTable' =>  function($sm) {
                    $tableGateway = $sm->get('PriceTableGateway');
                    $table = new PriceTable($tableGateway);
                    return $table;
                },
                'PriceTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Price());
                    return new TableGateway('price', $dbAdapter, null, $resultSetPrototype);
                },
                'Services\Model\TokenTable' =>  function($sm) {
                    $tableGateway = $sm->get('TokenTableGateway');
                    $table = new TokenTable($tableGateway);
                    return $table;
                },
                'TokenTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Token());
                    return new TableGateway('tokenPrices', $dbAdapter, null, $resultSetPrototype);
                },
                'Services\Model\ClientCoOrdinateTable' =>  function($sm) {
                    $tableGateway = $sm->get('ClientCoOrdinateTableGateway');
                    $table = new ClientCoOrdinateTable($tableGateway);
                    return $table;
                },
                'ClientCoOrdinateTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ClientCoOrdinate());
                    return new TableGateway('clientCoOrdinate', $dbAdapter, null, $resultSetPrototype);
                },
                'Services\Model\DriverCoOrdinateTable' =>  function($sm) {
                    $tableGateway = $sm->get('DriverCoOrdinateTableGateway');
                    $table = new DriverCoOrdinateTable($tableGateway);
                    return $table;
                },
                'DriverCoOrdinateTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DriverCoOrdinate());
                    return new TableGateway('driverCoOrdinate', $dbAdapter, null, $resultSetPrototype);
                },
                'Services\Model\TownsTable' =>  function($sm) {
                    $tableGateway = $sm->get('TownsTableGateway');
                    $table = new TownsTable($tableGateway);
                    return $table;
                },
                'TownsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Towns());
                    return new TableGateway('towns', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
}