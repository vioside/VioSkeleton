<?php
namespace Services\Model;

use Zend\Db\TableGateway\TableGateway;
use Services\Model\Driver;

class DriverTable
{
	protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function getDrivers($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('user_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    
    public function getDriversSC($secretcode)
    {
        $secretcode  = (string) $secretcode;
        $rowset = $this->tableGateway->select(array('secretcode' => $secretcode, 'secretcodeenable' => 1));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $secretcode");
        }
        return $row;
    }
    
    public function getDriversSCExists($secretcode)
    {
        $secretcode  = (string) $secretcode;
        $rowset = $this->tableGateway->select(array('secretcode' => $secretcode));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $secretcode");
        }
        return $row;
    }
    
    public function getDriversByClosest($sm, $userLat, $userLong, $vehicletype) {
		//drivers on call
		$adapter = $sm->get('Zend\Db\Adapter\Adapter');
		$stmt2 = $adapter->createStatement("SELECT DISTINCT driveraccepted FROM calls WHERE cancelled IS NULL and done IS NULL and expired IS NULL");
		$results2=$stmt2->execute();
		$i=0;
		$oncall = array();
		foreach($results2 as $r2) {
			$oncall[$i] = $r2["driveraccepted"];
			$i++;
		}
		//nearby drivers
		$adapter = $sm->get('Zend\Db\Adapter\Adapter');
		$stmt = $adapter->createStatement("SELECT user_id FROM driver WHERE nooftokens<>0 AND paid=1 AND approved=1 ORDER BY acos(sin(radians($userLat)) * sin(radians(lastLatitude) + cos(radians($userLat)) * cos(radians(lastLatitude)) * cos(radians(lastLongitude)-$userLong))) * 6371 desc");
		$results=$stmt->execute();

		$i = 0;
		$data = array();
		foreach($results as $result) {
			$incall = false;
			for($j=0; $j<count($oncall); $j++) {
				if($oncall[$j] == $result["user_id"]) {
					$incall = true;
				}
			}
			if(!$incall) {
				$r = new DriverId();
				$r->exchangeArray($result);
				$data[$i] = $r;
				$i++;
			}
		}

		return $data;
	}
	
	public function getDriverInfoByClosest($sm, $userLat, $userLong, $vehicletype) {
		//drivers on call
		$adapter = $sm->get('Zend\Db\Adapter\Adapter');
		$stmt2 = $adapter->createStatement("SELECT DISTINCT driveraccepted FROM calls WHERE cancelled IS NULL and done IS NULL and expired IS NULL");
		$results2=$stmt2->execute();
		$i=0;
		$oncall = array();
		foreach($results2 as $r2) {
			$oncall[$i] = $r2["driveraccepted"];
			$i++;
		}
		//nearby drivers
		$adapter = $sm->get('Zend\Db\Adapter\Adapter');
		$stmt = $adapter->createStatement("SELECT * FROM driver WHERE nooftokens<>0 AND paid=1 AND approved=1 ORDER BY acos(sin(radians($userLat)) * sin(radians(lastLatitude) + cos(radians($userLat)) * cos(radians(lastLatitude)) * cos(radians(lastLongitude)-$userLong))) * 6371 desc");
		$results=$stmt->execute();

		$i = 0;
		$data = array();
		foreach($results as $result) {
			$incall = false;
			for($j=0; $j<count($oncall); $j++) {
				if($oncall[$j] == $result["user_id"]) {
					$incall = true;
				}
			}
			if(!$incall) {
				$r = new Driver();
				$r->exchangeArray($result);
				$data[$i] = $r;
				$i++;
			}
		}

		return $data;
	}
	
	public function getAllDriversByClosest($sm, $userLat, $userLong, $vehicletype) {
		//nearby drivers
		$adapter = $sm->get('Zend\Db\Adapter\Adapter');
		$stmt = $adapter->createStatement("SELECT * FROM driver WHERE vehicletype=$vehicletype AND nooftokens<>0 AND paid=1 AND approved=1 ORDER BY acos(sin(radians($userLat)) * sin(radians(lastLatitude) + cos(radians($userLat)) * cos(radians(lastLatitude)) * cos(radians(lastLongitude)-$userLong))) * 6371 desc");
		$results=$stmt->execute();

		$i = 0;
		$data = array();
		foreach($results as $result) {
				$r = new Driver();
				$r->exchangeArray($result);
				$data[$i] = $r;
				$i++;
		}

		return $data;
	}

    
    public function editDrivers(Driver $driver)
	{
		$currentDriver = $this->getDrivers($driver->id);
		$currentDriver->id = 0;
		$currentDriver->emailBackup = $currentDriver->email;
		//generate a random number to use for email and username to avoid duplicate entry constraints
		$currentDriver->email = rand(1000000000000000, 9999999999999999) . '';
		$currentDriver->username = $currentDriver->email;
		$currentDriver->current = 0;
		$currentDriver->lastChangedDate = date("Y-m-d H:i:s");
	
		$this->saveDrivers($currentDriver);
		
		$driver->current = 1;
		$driver->firstAddedDate = $currentDriver->firstAddedDate;
		$driver->lastChangedDate = date("Y-m-d H:i:s");
		$this->saveDrivers($driver);
	}
	
    public function saveDrivers(Driver $driver)
    {
        $data = array(
            'username'  => $driver->username,
            'email'  => $driver->email,
            'emailBackup' => $driver->emailBackup,
            'display_name'  => $driver->display_name,
			'password' => $driver->password,
			'state' => $driver->state,
			'surname' => $driver->surname,
			'name' => $driver->name,
			'contactno' => $driver->contactno,
			'vehicleno' => $driver->vehicleno,
			'photo' => $driver->photo,
			'paid' => $driver->paid,
			'country' => $driver->country,
			'vehicletype' => $driver->vehicleType,
			'nooftokens' => $driver->noOfTokens,
			'lastlatitude' => $driver->lastLatitude,
			'lastlongitude' => $driver->lastLongitude,
			'secretcode' => $driver->secretCode,
			'secretcodeenable' => $driver->secretCodeEnable,
			'devicetoken' => $driver->devicetoken,
			'android' => $driver->android,
			'lastChangedDate' => $driver->lastChangedDate,
			'firstAddedDate' => $driver->firstAddedDate,
			'current' => $driver->current,
        );

        $id = (int)$driver->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getDrivers($id)) {
                $this->tableGateway->update($data, array('user_id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }
    
    public function deleteDrivers($id)
    {
        $this->tableGateway->delete(array('user_id' => $id));
    }
}